

var chartColors = {
  skyblue: 'rgba(0, 244, 254)'
};
var effectColors = {
  skyblue: 'rgb(42, 206, 240)',
  highlight: 'rgb(6, 129, 136)',
  shadow: 'rgba(16, 160, 196, 0.6)',
};

function randomScalingFactor() {
  return (Math.random() > 0.5 ? 1.0 : -1.0) * Math.round(Math.random() * 100);
}

var config = {
  type: 'line',
  data: {
    labels: ['Apr 02', 'Apr 03', 'Apr 04', 'Apr 05', 'Apr 06', 'Apr 07', 'Apr 08','Apr 09'],
    datasets: [{
      data:[200, 80, 280, 200, 620, 190, 1100, 320, 80],
      label: 'Dataset 1',
      backgroundColor: 'chartColors.skyblue',
      borderWidth: 1,
      borderColor: '#707070',
      pointBackgroundColor: chartColors.skyblue,
      // data: [0, 0, 0, 0, 0, 0, 0].map(function() {
      //   return randomScalingFactor();
      // }),
      fill: false,
      pointBorderWidth: 1,
      pointBorderColor: '#23A9DA',
      pointBevelWidth: 5,
      // pointBevelHighlightColor: effectColors.highlight,
      pointBevelShadowColor: effectColors.shadow,

      pointRadius: 10,
      pointHoverRadius: 12,

      pointHoverOuterGlowWidth: 20,
      pointHoverOuterGlowColor: effectColors.skyblue,

      pointOuterGlowWidth: 20,
      pointOuterGlowColor: effectColors.skyblue,

      pointInnerGlowWidth: 30,
      pointHoverInnerGlowWidth: effectColors.skyblue	
    }]
  },
  options: {
    responsive: true,
    title: {
      display: true,
    },
    scales: {
      xAxes: [{
        scaleLabel: {
          display: false,
          labelString: 'Month',
        },
        gridLines: {
            drawTicks: false,
            display: false
        },
        ticks: {
          padding: 15,
          fontStyle: "bold",
          fontSize: 14,
        }
      }],
      yAxes: [{
        scaleLabel: {
          display: false,
          labelString: 'Value'
        },
        gridLines: {
          drawBorder: false,
       },
        ticks: {
          max: 1600,
          padding: 15,
        }
      }]
    },
    legend: {
      display: false,
    },
    layout: {
      padding: {
        right: 20
      }
    }
  }
};

window.onload = function() {
  var ctx = document.getElementById('myChart').getContext('2d');
  window.myChart = new Chart(ctx, config);
};


const _days = document.getElementById('days');
const _hours = document.getElementById('hours');
const _mins = document.getElementById('mins');
const _secs = document.getElementById('secs');
  //set time countdown in here
var countDownDate = new Date("2020-11-04T14:45:00").getTime();

var x = setInterval(function () {
  let now = new Date().getTime();
            
  let distance = countDownDate - now;

 // Time calculations for days, hours, minutes and seconds
let days = Math.floor(distance / (1000 * 60 * 60 * 24));
let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
let seconds = Math.floor((distance % (1000 * 60)) / 1000);

// Output the result
_days.innerHTML = days < 10 ? '0' + days : days;
_hours.innerHTML = hours < 10 ? '0' + hours : hours;
_mins.innerHTML = minutes < 10 ? '0' + minutes : minutes;
_secs.innerHTML = seconds < 10 ? '0' + seconds : seconds;


// If the count down is over
if (distance < 0) {
  clearInterval(x);
  _days.innerHTML = '00';
  _hours.innerHTML = '00';
  _mins.innerHTML = '00';
  _secs.innerHTML = '00';
}}, 1000);




        
        
     